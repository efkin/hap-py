#!/usr/bin/python

from etherpad_lite import EtherpadLiteClient

API_KEY = ""

replace = {
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
}


def unescape(s, d):
    """ this function makes possible to use html inside the pad """

    result = s
    for k, v in d.iteritems():
        result = result.replace(v, k)
    return result


def template(body):
    """ base template to reuse """

    print "Content-type:text/html\r\n\r\n"
    print """
        <html>
        <head>
        <meta charset="UTF-8">
        <title>Hack the Earth II</title>
        <link rel="stylesheet" href="/style/style.css">
        </head>
        <body>
        """, body, \
        """
        </body>
        </html>
        """


def connect2api(pad):
    """ where the magic happens """
    # replace base_url of your local instance of etherpad
    # following the same pattern
    c = EtherpadLiteClient(base_params={'apikey': API_KEY},
                           base_url='http://etherpad.local/api')
    response = c.getHTML(padID=pad)
    result = response['html']
    parsed_result = unescape(result, replace)
    return parsed_result
