Description
-----------
This is hap-py (Html Asks from Pad). It is a little module to make static webpages from an etherpad in real-time. It is not designed with security in mind, but instead for use inside a local network where digital trustness exists.

Assumptions
-----------
* You know how to load CGI modules in your web server.
* You have an instance of etherpad-lite running in your local network or at least an API key.

Requirements
------------
* A web server running with cgi_module support enabled
* [Python-etherpad_lite](https://github.com/Changaco/python-etherpad_lite)

Instructions
------------
1. Insert at line 5 of hap-py/base.py the correspondant API key inside the quotes
2. Replace the 'base_url' parameter inside happy/base.py
3. Create as many modules you need importing hap-py module and calling relevant functions. (see example)
4. Change style/style.css as you wish
5. Enjoy collaborative editing of a web page.
